.TH PTILE 1 "2015 Feb 8" "Geometry Center/MoMath"
.SH NAME
ptile \- Interactively build 2-D tiling
.SH SYNOPSIS
\fBptile\fP
[\fIfile.pg\fP]
.SH DESCRIPTION
\fBPtile\fP lets the user interactively build a tiling, using
Tcl/Tk version 4.  Tilings are described by \fB.pg\fP files,
described below.

The window displays a collection of \fBpatches\fP, each comprising
one or more polygonal \fBtiles\fP.  Glued tiles are attached edge-to-edge
(or edge-midpoint to edge-midpoint) with other tiles; typically one glues
edges of the same length together when working with ptile, although that is
not enforced. One glues patches together, not by
dragging them near each other, but by marking edges that are to be joined.
\fBPtile\fP knows nothing of matching rules; it permits gluing any edge to
any other.

The user can copy, paste and duplicate patches of tiles. There can be an
"active edge," ready for gluing onto other tiles, indicated by a triangle
attached to the edge in the direction it will be glued.  When an edge is active,
some operations apply to its entire patch, idicated by thicker borders.
Whenver tiles are glued, they are added to the destination patch.

New tiles can be designed by specifying edge-lengths and angles; also,
\fB\.pg\fP files have a simple ASCII format easily written by other programs.

.SH "Mouse Commands"

Though the commands mentioned here assume a three-button mouse, those using
the middle or right buttons have keyboard synonyms, described below.
Hence, it is possible to use \fBptile\fP with a single-button mouse.

.IP "Left mouse: Select, or add a single tile"
Click on an unoccupied region: deactivate edge, selection unaffected.

Click (drag) on tile(s): select (rectangle of) tile(s), deactivate edge.
(To unselect all, drag on an empty region.)

Click on an edge: The selection is unaffected.  If there is no active edge,
activates the edge.  If there is an active edge, glues
a copy of the tile containing the active edge to the clicked edge.
(But clicking on the active edge just deactivates it.)

.IP "Shift-left: Toggle select"
Shift-left-mouse toggles selection status for the tiles you touch,
while leaving other tiles alone.  So, to select a complicated or disconnected
region of tiles, hold down the shift key and click or drag.

.IP "Control-left (or \fBc\fP key): Select tile & choose color"
Clicking the left button while holding the Control key selects the
tile under the cursor, and also sets the current color (for future tiles)
from the tile's color.

.IP "Double-left: Glue/paste tiles"
Double-click on unoccupied region: paste a copy of the selected tiles,
as a new patch; deactivate edge

Double-click on a tile: select the entire patch containing that tile;
deactivate edge

Double-click on an edge: The selection is unaffected. If there is no
active edge, activates the edge. If there is an active edge, glues
the entire patch containing the active edge onto the clicked edge. The
glued-from patch is rotated while copying, so that the orientations of
the glued-from and glued-to edges match.  The midpoints of the corresponding
edges are aligned. The glued-from tile may be in the same patch as the
glued-to tile.

Gluing may cause tiles to overlap; \fBptile\fP does not attempt to
prevent that.  Use the \fBu\fP key to undo gluing.

.IP "Button 2 (or \fBm\fP key): Move Patch or Selection / Scroll View"
Note button 2 is the right button on Macs, but may be the middle button on
other operating systems.  Dragging with button 2 pressed has the
following effects:

When nothing is selected or active: scroll viewport

When an edge is active: move the patch that edge is on

When tiles are selected but no patch is active: move just the selected
tiles, creating a new patch.  This allows you to split existing large patches

To invoke via the keyboard, press \fBm\fP, then click and drag.
After releasing the mouse button, its function returns to normal.

.IP "Double-middle (or \fBz\fP key): Zoom view"
Double-clicking, then dragging with button 2 scales the view.

.IP "Button 3 (or \fBg\fP key): Glue/paste tiles"
Note button 3 is the middle button on Macs, right on other systems.
All button-3 single clicks are the same as a button 1 double-click, except they
do not affect edge activation; drags have no effect.

Shift-clicks are the same, except the gluing occurs rotated 180 degrees

.SH "Keyboard Commands"

.IP "\fBbackspace\fP: Delete selected tiles"
The \fBbackspace\fP key deletes any selected tiles; so do the \fBdelete\fP and
\fBx\fP keys.  Use the \fBu\fP key to undo deletion.

.IP "\fBv\fP: Paste selected tiles"
The \fBv\fP key pastes a copy of any selected tiles to create a new patch.
The pasted tiles appear under the cursor.
Use the \fBu\fP key to undo pasting, discarding the newly created patch.

.IP "\fBc\fP: choose color from tile under cursor"
The topmost tile under the cursor, if any, is selected,
and its color becomes the current color.
Synonymous with the Control-Left-button command.

.IP "\fBf\fP: fill selected tiles with color"
Pressing the \fBf\fP key fills any selected tiles with the current color.

.IP "\fBm\fP: move patch/scroll view"
Press \fBm\fP, then drag with the left mouse button to move the selected patch,
if any, or otherwise scroll the view.
Synonymous with clicking once with the middle mouse button.  

.IP "\fBz\fP: zoom view"
Press \fBz\fP, then drag with the left button to zoom the view.
A synonym for double-clicking the middle mouse button.

.IP "\fBw\fP: recenter view"
Press \fBw\fP to scroll so that the middle of the collection of tiles
is centered on the window.

.IP "\fBW\fP: view everything"
Press \fBW\fP to scale and adjust the view so that all tiles are just visible.

.IP "\fBg\fP: glue copy of patch"
First, select a source edge of some patch by clicking the left mouse
button; then choose the corresponding edge of the destination patch by pointing
the cursor at another edge and pressing \fBg\fP (without clicking).
A copy of the source patch
is glued onto the destination patch, with the source copy rotated so that
their edges match and midpoints are aligned.  Pressing \fBg\fP is a synonym
for clicking the right mouse button.

.IP "\fBG\fP: glue 180-degree rotated copy of patch"
Like \fBg\fP, but rotates 180 degrees before gluing.

.IP "\fBd\fP: design new tile"
See the description of the \fBDesign Tile\fP menu option, below.

.IP "\fBu\fP: Undo"
Typing \fBu\fP undoes the most recent operation: gluing one patch
onto another, pasting a new patch, or deleting tiles.  Type \fBu\fP
again to re-do the undone operation.

.IP "\fB<\fP: Import .pg file"
Opens a file browser to import a ptile file, whose suffix is .pg by
convention.  The browser shows only .pg files, though you can type any
name into the text-entry box given.

The file's tiles are added to the current scene.  To load a new file
from scratch, use the \fBRevert\fP menu option.

.IP "\fB>\fP: Save .pg file"
Opens a file browser to save the current scene as a ptile file.
It's best to use the suffix \fB.pg\fP, though you'll need to type this
explicitly in the text box -- ptile doesn't add it automatically.

.SH "Other Options"
The menu bar at the top of the window includes a \fBFile\fP menu,
a color well, a message area, and (when an appropriate command is invoked)
a one-line text-entry field for entering names of files or colors.

.IP "Color well"
Clicking on the color well (the colored block in the menu bar above the
window) allows you to type in the name or hexadecimal code of a color,
as in CornflowerBlue or #32A88D, in standard X style.  Color names
are listed in the file \fB/usr/lib/X11/rgb.txt\fP, if you're running X.
(Pick colors whose names don't contain blanks.)  The color may also be set
by clicking control-left-mouse-button over a tile.
To leave the current color unchanged, clear the input field before
pressing \fBReturn\fP.

.IP "File Menu"
Keyboard commands ``\fB<\fP'' and ``\fB>\fP''
are synonyms for \fBImport\fP and \fBSave\fP, respectively.

(If you don't see the new tiles, try zooming out or using the "W" key command.)

.IP "New"
Use \fBNew\fP to start afresh with a single pentagon.

.IP "Design Tile"
Use \fBDesign Tile\fP to create a new tile.  You're prompted for a
series of edge-lengths (with \fB1.0\fP being the current standard edge length)
and interior angles (in degrees), separated by blanks.  Don't specify
the final edge; its orientation and length are chosen to close the polygon.
Specifically, to create an N-gon, you'll enter
.br
  \fIlength1 angle1 length2 angle2 ... angleN-2 lengthN-1\fP
.sp
For example, to create an equilateral triangle, give the lengths of the
first two edges and intervening angle:
.br
.ti +5
1  60  1
.sp
To create a Penrose kite:
.br
.ti +5
1.61803  72  1  144  1
.br
Or, a Penrose dart:
.br
.ti +5
1.61803  36  1  216  1
.br
For convenience, the edge-lengths and angles may be Tcl expressions (so long
as they contain no blanks), e.g.
.br
.ti +5
1 45 1/sqrt(2)
.br
For convenience, \fB$tau\fP is available as a synonym for the golden ratio
(sqrt(5)+1)/2, so a Penrose kite is also
.br
.ti +5
$tau 72 1 144 1
.br
The new tile is created with one corner under the cursor (or in the center
of the window, if the cursor is outside the drawing area).
It's initially selected, and ptile is in move mode as if you'd just typed
\fBm\fP; drag with the left mouse button to move the new patch.

.IP "Center view"
adjusts the view, centering the set of tiles on it.

.IP "View all"
adjusts and scales the view so that all tiles are just visible.

.IP "Print"
displays an lpr command; add -P\fIprintername\fP to choose a
non-default printer, or if you use something other than lpr to print.
The printer receives a Postscript copy of whatever is currently
visible in the window.  Printing only works on Unix systems.

.IP "Save PS"
saves the current view as Postscript, for later printing.

On the Macintosh, if you have a Postscript-capable printer, you can send
Postscript files to it with Apple's \fBLaserwriter Utility\fP program,
available from
.br
ftp://ftptoo.support.apple.com//pub/apple_sw_updates/US/Macintosh/Printing%20
Software/LaserWriter%20Software/LaserWriter%20Utility%20%287.6.2%29.hqx

A stripped-down version of \fBLaserWriter Utility\fP, here called
\fBPrint Postscript File\fP, is included in the ptile package for the Mac.

.SH HINT
The tiling area may be much larger than the window.  To get an overview,
or if you get lost, try the \fBw\fP or \fBW\fP keys.

.SH "FILE FORMAT"
Input files for \fBptile\fP are ASCII; it should be fairly simple to write
programs to create them.

The first line of a \fBptile\fP file should be:
.br
.ti +5
# polygons v0.2
.br

Following this is a line giving the typical length of an edge in this file:
.br
.ti +5
unit \fIN\fP
.br

It's not essential that all edges actually be that length; the value \fIN\fP
is used when combining files (e.g. reading a file with \fBOpen\fP while
some tiles are already on the screen), to match the scaling of
independently-created files.

Then follows a sequence of patches, each with a sequence of polygons.
A patch begins with a line reading simply
.br
.ti +5
patch
.br
and is followed by a sequence of lines of the form
.br
.ti +5
polygon \fIx1 y1 x2 y2 ... xn yn\fP [\-fill \fIcolor\fP]
with the ``\-fill \fIcolor\fP'', if omitted, defaulting to the current color,
which may be set with the paintcolor command below.

It's not important whether \fIx1 y1\fP equals \fIxn yn\fP.

Order of vertices of a polygon matters, since edge-orientations are
matched when patches are glued together.  By convention, vertices are arranged
counterclockwise around the polygon, such that arctan((y-y0)/(x-x0)) increases
from one vertex to the next, where (x0,y0) is some point inside the polygon.
(Since Tcl/Tk uses a left-handed coordinate system, with Y increasing down,
this implies that polygon vertices appear clockwise as drawn on the screen.)
Color names are standard X colors, as mentioned under \fBColor well\fP, above.

Files may also include line segments, as in
.br
.ti +5
line \fIx1 y1 x2 y2 ... xn yn\fP [\-fill \fIcolor\fP] [\-width \fIwidth\fP]
.br
Line widths are in pixels, default 1.  Note that line widths aren't
scaled when tiles are scaled.  Interactive facilities for placing
lines aren't good; they're mainly useful as decorations for polygonal tiles.

Files for \fBptile\fP may include comments (from \fB#\fP to the end of its line)
and blank lines, though these aren't preserved when saving.

Additional commands that may appear in pg files:
.br
.ti +5
paintcolor \fIcolor-spec\fP
.br
Sets the default color for future tiles.

.br
.ti +5
design \fIx y l1 a1 ... ln-1 an-1 ln \fP 
.br

Generate a tile in the current default color, at position (x,y), with the
alternating length/angle notation as in the Design Tile section above.

.SH AUTHORS
Stuart Levy (slevy@geom.umn.edu), Geometry Center, September, 1995.

National Museum of Mathematics, info@momath.org, January, 2015

.SH BUGS
There ought to be more handy tools for creating new tile shapes.
Importing from Adobe Illustrator would be nice.

If you delete some tiles, zoom, and undo, you get the tiles back but at a
different scale.
