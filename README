This is ptile, an interactive 2-D tiling editor, built using
tcl/tk version 4.0 or later, written by Stuart Levy, Geometry Center,
University of Minnesota (slevy@geom.umn.edu), and enhanced by the
National Museum of Mathematics.  It's available (as of April, 1997)
by anonymous FTP from geom.umn.edu as priv/slevy/ptile.tar.gz
and (for the Mac) as priv/slevy/ptile.sea.hqx, and as of Jan 2015 via a
publicly-clonable git repository at gitlab.momath.org.

UNIX and Windows users will need to have tcl/tk, with tk version 4.0 or later,
installed to run it.  That package is available in source from from ftp.smli.com
in the directory /pub/tcl.  They also have tcl/tk installations for Windows
and MacOS in source and binary form.  It is generally included in recent
distributions of MacOS X

This version is patched by the National Museum of Mathematics to run
on the wish (Tcl/Tk 8.5.9) that comes pre-installed on Mac OS X 10.10, in
early 2015. It also has numerous enhancments as compared to previous
versions of ptile. There may have been incompatible changes; this revised
ptile may no longer run on previous versions of Tcl/Tk.


	INSTALLATION


This package includes these files:

     ptile		tcl/tk tiling editor
     ptile.1		man page in UNIX nroff format
     penrose.pg		Sample Penrose tiles (kite/dart, rhombs, triangles)
     patternblocks.pg	Pattern Blocks sample tiles
     pentagons.pg	Some pentagons; a medium-sized tiling
     quasi.pg		Section of Penrose rhomb tiling, from QuasiTiler
     unstraightenable.pg Section of a quasiperiodic tiling whose
			dual lines can't be straightened,
			by Marjorie Senechal of Smith College
     quasi2pg		perl program, converts QuasiTiler PS pictures
			into ptile .pg files
     CairoPrismatic.pg  Frank Morgan's Cairo and prismatic tiles, see
     			http://sites.williams.edu/Morgan/2015/01/31/new-optimal-pentagonal-tilings/#more-1842


  UNIX/Macintosh Installation


On Unix systems, simply copy ptile into some directory on users' search
paths, e.g.:

   cp ptile  /usr/local/bin/ptile

ptile assumes that the Tk "wish" interpreter is in "/usr/local/bin/wish";
if it's elsewhere, edit "ptile" and change its first line.

You can also simply execute "wish ptile" in the directory in which the ptile
file resides.

Likewise, copy quasi2pg into /usr/local/bin or similar, and edit its first
line if perl is installed somewhere other than /usr/local/bin/perl.
quasi2pg reads Postscript files returned by the QuasiTiler WWW application,
(http://www.geom.umn.edu/apps/quasitiler/, with PostScript Only selected)
and writes corresponding ptile files.

To generate the documentation in various formats:

   groff -man -Tps ptile.1 > ptile.ps
   groff -man -Tlatin1 > ptile.man
   groff -man -Thtml > ptile.html
   
  Windows Installation

To run under Windows, try this.  FTP to ftp.smli.com, cd to pub/tcl,
and (in binary mode) fetch the latest win<whatever>.exe, currently win42.exe.
Run it under Windows.  It should ask you where (under what directory)
to install Tcl/Tk.  The directory must already exist, so create it if need be.
On our system, I picked "c:\apps\tcltk".  Once the installation is complete,
the main "wish" interpreter will be (in this example)
	c:\apps\tcltk\bin\wish42.exe

Then, install the ptile script and its siblings (penrose.pg, etc.) somewhere.
On our system, I created a directory "c:\apps\ptile", so the script is
called "c:\apps\ptile\ptile".

Now, create a file "ptile.bat" somewhere, containing just one line:

c:\apps\tcltk\bin\wish42.exe  c:/apps/ptile/ptile  %1

  (Note the forward-slashes "/" replacing backslashes "\" as path
   separators in the name of the script.  This is important!
   Also, of course, change the above to mention the places where
   the "wishXX.exe" executable and "ptile" were actually placed.)

Finally, make Windows realize that the new "ptile.bat" file is the
way to invoke .pg files.   Under Windows 95, the way to do that seems to be:
   Find (or create) a .pg file somewhere, and double-click on in the file
   browser.  It will display a list of possible applications; click
   the "Other" button to get a file browser.  Use that to locate the ptile.bat
   file you've created, and select it.
Under Windows 3.1, I'm told the way to do that involves opening the
   Control Panel.  Somewhere in it is some sort of preferences list, giving
   the application to be invoked for various file suffixes.

Once this is done, you can just double-click on any .pg file to open it.
